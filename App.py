from copy import deepcopy
from datetime import datetime
from json import dumps
from os import makedirs
from os.path import basename
from os.path import join as path_join
from time import sleep
from typing import List
from typing import Optional

from RPi import GPIO
from cv2 import CAP_PROP_FRAME_HEIGHT
from cv2 import CAP_PROP_FRAME_WIDTH
from cv2 import VideoCapture
from cv2 import imwrite
from imutils import rotate_bound

settings: 'Settings'
log: 'Log'
agent: 'Agent'
cam: Optional[VideoCapture] = None


class MetadataOverflow(Exception):
    pass


def transmit_image(image_file: str, transmission_topic: str):
    metadata: List[str] = [
        "line_monitor",
        basename(image_file).split(".")[-1],
        basename(image_file).rsplit(".", 1)[0],
        ""
    ]

    str_metadata: str = "\x10".join(metadata)
    if len(str_metadata) > 127:
        raise MetadataOverflow(f"{len(str_metadata):x}>{127:x}")
    bin_metadata: bytes = f"{chr(len(str_metadata))}{str_metadata}".encode()

    bin_file: bytes
    with open(image_file, "rb") as f:
        bin_file = f.read()

    bin_data: bytes = bin_metadata + bin_file

    while not agent.is_connected():
        agent.loop(1)
        sleep(0.5)

    agent.publish(transmission_topic, bin_data)


def capture_image(image_name: str) -> str:
    if not cam.isOpened():
        return ""
    image_file = path_join(settings["folders"]["data"], "line_monitor", image_name)
    frame = cam.read()[1]
    if settings["line_monitor"]["camera_angle"]:
        frame = rotate_bound(frame, int(settings["line_monitor"]["camera_angle"]))
    imwrite(image_file, frame)
    return image_file


def set_resolution(cap: VideoCapture, x: int, y: int):
    cap.set(CAP_PROP_FRAME_WIDTH, x)
    cap.set(CAP_PROP_FRAME_HEIGHT, y)


def button_event(pin: int):
    pin_str: str = str(pin)

    image_file: str = capture_image(
        f"{settings['line_monitor']['buttons'][pin_str]}_{datetime.now().isoformat().replace(':', '.')}.png"
    )

    settings.set(
        settings["line_monitor"]["totals"].get(pin_str, 0) + 1,
        "line_monitor", "totals", pin_str
    )

    log.write(
        f"LINE MONITOR EVENT:{pin}" +
        f":{settings['line_monitor']['buttons'][pin_str]}" +
        f":{settings['line_monitor']['totals'][pin_str]}"
    )

    agent.publish(
        "button" + "/" + settings["line_monitor"]["buttons"][pin_str],
        settings["line_monitor"]["totals"][pin_str]
    )

    transmit_image(image_file, settings["line_monitor"]["image_topic"])


def callback_control(ag: 'Agent', __, msg_b: 'mqtt.MQTTMessage'):
    msg: str = msg_b.payload.decode()
    log.write(f"LINE MONITOR CONTROL:{msg}")

    rep_log: str = ""
    rep_pub: str = ""

    if msg == "totals":
        rep_pub = dumps(settings["line_monitor"]["totals"])
    elif msg == "reset":
        settings.set({}, "line_monitor", "totals")
        rep_log = rep_pub = "LINE MONITOR:RESET"
    elif msg == "capture":
        image_file: str = capture_image(f"manual_{datetime.now().isoformat().replace(':', '.')}.png")
        if image_file:
            transmit_image(image_file, settings["line_monitor"]["image_topic"])
            rep_log = rep_pub = f"LINE MONITOR:MANUAL CAPTURE"
        else:
            rep_log = rep_pub = f"LINE MONITOR:MANUAL CAPTURE ERROR"
    else:
        rep_log = rep_pub = f"LINE MONITOR CONTROL ERROR:UNKNOWN COMMAND:{msg}"

    log.write(rep_log) if rep_log else None
    ag.publish("console", rep_pub) if rep_pub else None


def app(settings_arg: 'Settings', log_arg: 'Log', agent_arg: 'Agent'):
    global settings
    global log
    global agent
    global cam

    try:
        settings = settings_arg
        log = log_arg
        agent = agent_arg
        cam = VideoCapture(settings["line_monitor"]["camera_index"])
        set_resolution(
            cam,
            settings["line_monitor"]["camera_resolution"]["x"],
            settings["line_monitor"]["camera_resolution"]["y"]
        )

        makedirs(path_join(settings["folders"]["data"], "line_monitor"), exist_ok=True)

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BOARD)

        for pin in settings['line_monitor']['buttons']:
            GPIO.setup(int(pin), GPIO.IN, pull_up_down=GPIO.PUD_UP)
            GPIO.add_event_detect(int(pin), GPIO.FALLING, callback=button_event, bouncetime=200)
            log.write(f"LINE MONITOR:BUTTON ADD:{pin}")
            agent.publish("console", f"LINE MONITOR:BUTTON ADD:{pin}")

        agent.subscribe(settings["line_monitor"]["control_topic"])
        agent.message_callback_add(settings["line_monitor"]["control_topic"], callback_control)

        settings_prev = deepcopy(settings["line_monitor"])
        del settings_prev["totals"]
        while True:
            agent.loop(0.5)
            settings_curr = deepcopy(settings["line_monitor"])
            del settings_curr["totals"]
            if settings_curr != settings_prev:
                log.write("LINE MONITOR:SETTINGS CHANGED:RESTART")
                agent.publish("console", "LINE MONITOR:SETTINGS CHANGED:RESTART")
                break
    finally:
        if cam is not None:
            cam.release()
        GPIO.cleanup()
