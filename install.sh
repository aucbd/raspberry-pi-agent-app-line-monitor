install_debian() {
  local packages=""
  dpkg -s python3-opencv || packages+="python3-opencv "
  dpkg -s python3-rpi.gpio || packages+="python3-rpi.gpio "
  if [[ -n $packages ]]; then
    sudo apt update
    sudo apt install --assume-yes "${packages% }"
    return $?
  fi
  return 0
}

if command -v apt 1>/dev/null 2>/dev/null; then
  install_debian
fi

return $?
